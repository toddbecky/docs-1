---
title: About the JavaScript API
platform: cloud
product: jsdcloud
category: reference
subcategory: jsapi
date: "2017-08-24"
---
{{< include path="docs/content/cloud/connect/concepts/javascript-api.snippet.md">}}