---
title: "Deprecation notice - Basic authentication with passwords and cookie-based authentication"
platform: cloud
product: jiracloud
category: devguide
subcategory: updates
date: "2018-03-01"
---

# Deprecation notice - Basic authentication with passwords and cookie-based authentication

<div class="aui-message note">
    <div class="icon"></div>
    <p>
        <strong>The six month deprecation period for these changes began on 15 March 2018.</strong>
    </p>
    </br>
    <p>We plan to remove support for these authentication methods by December 1 2018. Follow the ACJIRA tickets linked below to stay up to date.
    </p>
    <br/>
</div>

Atlassian has introduced support for [API tokens] for all Atlassian Cloud sites as a replacement 
for [basic authentication] requests that previously used a password or primary credential for an Atlassian 
account, as well as [cookie-based authentication].

Basic authentication with passwords and cookie-based authentication are now deprecated and will be removed later in 2018 in 
accordance with the [Atlassian REST API policy].

## What is changing?

If you currently rely on a script, integration, or application that makes requests to Jira Cloud with basic or cookie-based 
authentication, you should update it to use basic authentication with an API token, OAuth, or Atlassian Connect as soon as 
possible.

[Learn more about creating API tokens].

## Why is Atlassian making this change?

Using primary account credentials like passwords outside of Atlassian's secure login screens creates security risks for our 
customers. API tokens allow users to generate unique one-off tokens for each app or integration that can be easily revoked if 
needed.

Furthermore, as customers adopt features like two-factor authentication and SAML, passwords may not work in all cases. API tokens have been specifically designed as an alternative for Atlassian accounts in organizations that are using these features.

## Which APIs and methods will be restricted?

For the following APIs and pages, all requests using basic authentication with a non-API token credential will return 401 (
Unauthorized) after the deprecation period:

* Jira Cloud public REST API
* Jira Software public REST API
* Jira Service Desk public REST API
* All Jira Cloud web pages

The following Jira Cloud REST API endpoint will be removed:

* [`/rest/auth/1/session`](/cloud/jira/platform/rest/#api-auth-1-session-get)

### How can I be notified when these authentication methods will be removed?

Please watch [ACJIRA-1465] to be notified of changes to basic authentication and [ACJIRA-1466] to be notified of changes to 
cookie-based authentication.

[API tokens]: https://confluence.atlassian.com/cloud/api-tokens-938839638.html
[basic authentication]: /cloud/jira/platform/jira-rest-api-basic-authentication
[cookie-based authentication]: /cloud/jira/platform/jira-rest-api-cookie-based-authentication/
[Atlassian REST API policy]: /platform/marketplace/atlassian-rest-api-policy/
[Learn more about creating API tokens]: https://confluence.atlassian.com/cloud/api-tokens-938839638.html
[ACJIRA-1465]: https://ecosystem.atlassian.net/browse/ACJIRA-1465
[ACJIRA-1466]: https://ecosystem.atlassian.net/browse/ACJIRA-1466