---
aliases:
    - /jiracloud/conditions.html
    - /jiracloud/conditions.md
    - /cloud/jira/platform/conditions.html
    - /cloud/jira/platform/conditions.html
title: Conditions
platform: cloud
product: jiracloud
category: devguide
subcategory: blocks
date: "2017-08-24"
---
{{< include path="docs/content/cloud/connect/concepts/jira-conditions.snippet.md">}}
