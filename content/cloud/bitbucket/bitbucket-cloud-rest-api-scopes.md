---
title: "Bitbucket Cloud REST API scopes"
platform: cloud
product: bitbucketcloud
category: reference
subcategory: api 
date: "2015-09-17"
---

# Scopes for the Bitbucket Cloud REST API

Bitbucket's API applies a number of privilege scopes to endpoints. In order to access an endpoint, a request will need to have the necessary scopes.

Scopes are declared in the descriptor as a list of strings, with each string being the name of a unique scope.

A descriptor lacking the `scopes` element is implicitly assumed to require all scopes and as a result, Bitbucket will require end users authorizing/installing the app to explicitly accept all scopes.

Our best practice suggests you add the scopes your app needs, but no more than it needs.

Invalid scope strings will cause the descriptor to be rejected and the installation to fail.

Following is the set of all currently available scopes.

### repository

Gives the app read access to all the repositories the authorizing user has access to.
Note that this scope does not give access to a repository's pull requests.

* access to the repo's source code
* clone over https
* access the the file browsing API
* download zip archives of the repo's contents
* the ability to view and use the issue tracker on any repo (created issues, comment, vote, etc)
* the ability to view and use the wiki on any repo (create/edit pages)

### repository:write

Gives the app write (not admin) access to all the repositories the authorizing user has access to. No distinction is made between public or private repos. This scope implies `repository`, which does not need to be requested separately.
This scope alone does not give access to the pull requests API.

* push access over https
* fork repos

### repository:admin

Gives the app admin access to all the repositories the authorizing user has access to. No distinction is made between public or private repos. This scope does not imply `repository` or `repository:write`. It gives access to the admin features of a repo only, not direct access to its contents. Of course it can be (mis)used to grant read access to another user account who can then clone the repo, but repos that need to read of write source code would also request explicit read or write.

This scope comes with access to the following functionality:

* view and manipulate committer mappings
* list and edit deploy keys
* ability to delete the repo
* view and edit repo permissions
* view and edit branch permissions
* import and export the issue tracker
* enable and disable the issue tracker
* list and edit issue tracker version, milestones and components
* enable and disable the wiki
* list and edit default reviewers
* list and edit repo links (Jira/Bamboo/Custom)
* list and edit the repository web hooks
* initiate a repo ownership transfer

### snippet

Gives the app read access to all the snippets the authorizing user has access to.
No distinction is made between public and private snippets (public snippets are accessible without any form of authentication).

* view any snippet
* create snippet comments

### snippet:write

Gives the app write access to all the snippets the authorizing user can edit.
No distinction is made between public and private snippets (public snippets are accessible without any form of authentication).
This implies the Snippet Read scope which does not need to be requested separately.

* edit snippets
* delete snippets

### issue

Ability to interact with issue trackers the way non-repo members can.
This scope does not imply any other scopes and does not give implicit access to the repository the issue is attached to.

* view, list, and search issues
* create new issues
* comment on issues
* watch issues
* vote for issues

### issue:write

This implies `issue`, but adds the ability to transition and delete issues.
This scope does not imply any other scopes and does not give implicit access to the repository the issue is attached to.

* transition issues
* delete issues

### wiki

Gives access to wikis. No distinction is made between read and write as wikis are always editable by anyone.
This scope does not imply any other scopes and does not give implicit access to the repository the wiki is attached to.

* view wikis
* create pages
* edit pages
* push to wikis
* clone wikis

### pullrequest

Gives the app read access to pull requests and collaborate on them.
This scope implies `repository`, giving read access to the pull request's destination repository.

* see and list pull requests
* comment on pull requests
* approve pull requests
* create and resolve tasks

### pullrequest:write

Implies `pullrequest` but adds the ability to create, merge, and decline pull requests.
This scope implies `repository:write`, giving write access to the pull request's destination repository. This is necessary to facilitate merging.

* merge pull requests
* decline pull requests
* create pull requests

### email

Ability to see the user's primary email address. This should make it easier to use Bitbucket Cloud as a login provider to apps or external applications.

### account

Ability to see all the user's account information. Note that this does not include any ability to mutate any of the data.

* see all email addresses
* language
* location
* website
* full name
* SSH keys
* user groups

### account:write

Ability to change properties on the user's account.

* delete the authorizing user's account
* manage the user's groups
* manipulate a user's email addresses
* change username, display name and avatar

### team

The ability to find out what teams the current user is part of. This is covered by the teams endpoint.

* information about all the groups and teams of which I am a member or admin


### team:write

Implies `team`, but adds the ability to manage the teams that the authorizing user is an admin of.

* manage team permissions

### webhook

Gives access to webhooks. This scope is required for any webhook
related operation.

This scope gives read access to existing webhook subscriptions on all
resources you can access, without needing further scopes. This means that
a client can list all existing webhook subscriptions on repository
`foo/bar` (assuming the principal user has access to this repo). The
additional `repository` scope is not required for this.

Likewise, existing webhook subscriptions for a repo's issue tracker can be
retrieved without holding the `issue` scope. All that is required is the
`webhook` scope.

However, to create a webhook for `issue:created`, the client will need to
have both the `webhook` as well as `issue` scope.

* list webhook subscriptions on any accessible repository, user, team, or snippet
* create/update/delete webhook subscriptions
