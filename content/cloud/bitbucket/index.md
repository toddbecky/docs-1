---
title: "Latest updates"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: index
date: "2017-04-24"
---

# Latest updates

We deploy updates to Bitbucket Cloud frequently. As a Bitbucket developer, it's important that you're aware of the changes. The resources below will help you keep track of what's happening.

## Atlassian Developer blog

In the **Atlassian Developer blog** you'll find handy tips and articles related to Bitbucket development.

Check it out and subscribe here: [Atlassian Developer blog](https://developer.atlassian.com/blog/categories/bitbucket/) *(Bitbucket-related posts)*.

## Bitbucket blog

Major changes that affect all users of the Bitbucket Cloud products are announced in the **Bitbucket blog**. This includes new features, bug fixes, and other changes.

Check it out and subscribe here: [Bitbucket blog](https://blog.bitbucket.org/).


