---
Title: "Patterns and examples"
product: bitbucketcloud
category: devguide
layout: patterns
---

{{< include path="docs/content/cloud/bitbucket/snippets/bitbucket-patterns-and-examples.snippet.md" >}}
