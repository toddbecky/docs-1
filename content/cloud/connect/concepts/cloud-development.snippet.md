# Understanding Atlassian in the cloud

Atlassian Connect is built to integrate with our hosted applications like Jira and Confluence Cloud. 
This page explains basic architecture, purchasing, licensing, and development concerns as you build 
your app. 

## <a id="overview"></a>Architecture overview

Even though cloud products can be packaged together for customers, each Jira and Confluence Cloud account 
is a separate instance.

Although each application is isolated from a security perspective, underlying resources like hardware,
CPU, and memory can be shared between many customers. Servers for our cloud applications are 
[located in the US](https://www.atlassian.com/hosted/security), and will soon be 
[available in Europe](https://www.atlassian.com/trust/infrastructure) as well.

Each Jira or Confluence Cloud instance is identifiable by its tenant ID. An instance URL is liable to change 
without warning. 

Each cloud instance has a set of licensed users. For these users:

* __Email addresses are unique within an instance, but may be used across multiple instances.__  
* __Users are identified by key, rather than name or email.__ Keys are also unique within an
	instance but may not be unique across instances.  

Your app is automatically granted a new bot user in cloud instances. These "users" appear in the 
user management portal, but don't count against actual user licenses. This user profile is assigned to two 
groups by default: _atlassian-addons_ and _product-users_ (like _jira-users_ or _confluence-users_). Customers 
should **not** remove bot users from these groups.  

Your app accesses cloud instances through the [Universal Plugin Manager](https://confluence.atlassian.com/x/8AJTE). 
Admins install your app by registering your descriptor into an instance. App installation and licensing are separate concerns. 

It's possible for a cloud instance to have your descriptor installed, but not to have a valid license. 

You won't receive any communication from instances that don't have your app descriptor installed. As 
expected, you're unable to communicate with instances that don't have your descriptor installed. 

## <a id="restarts"></a>Software upgrades

Cloud instances restart regularly during [maintenance windows](https://confluence.atlassian.com/x/aJALE). 
Weekly releases may or may not contain updates to cloud applications or other components. Generally, 
Jira and Confluence update versions every other week. That said, even if a product doesn't have an 
update, it may still be restarted. 

Instances also occasionally restart outside of these windows to recover from errors, or facilitate 
support. After restarting, initial requests to these instances may have higher latency as caches 
are repopulated. 

## <a id="purchasing"></a>Purchasing & licensing  

When cloud product customers choose a new product or app, they automatically enter a free 
trial period. This trial lasts 30 days, plus the time until their next bill. This means the 
actual trial period is between 31 and 61 days, with an average of 45 days. 

Customers can choose to subscribe to products and apps on a monthly or annual basis, and can 
cancel accounts or apps. Canceled accounts remain valid and active until the end of the billing 
period. 

We remove cloud product data 15 days after cancellation. For this reason, publish your own data retention 
policy. 
 
Since app installation and licensing are handled separately, always check the 
[license status](/platform/marketplace/cloud-app-licensing/) on each request and serve an appropriate
response.

## <a id="development"></a>Development checklist

Develop your app with the following concepts in mind: 

* Ensure the app descriptor base URL starts with HTTPS.
* Only serve content over HTTPS. 
* Test and support your app for [cloud application-supported browsers](https://confluence.atlassian.com/cloud/supported-browsers-744721663.html).
* Use [localization parameters](../internationalization/) with each request to serve content appropriate languages. 
* Store user data against an identifier combined from tenant ID and user key. This prevents accidental sharing of data between tenants or users.
* [Publish a security statement](/platform/marketplace/cloud-security-program/) about your data storage practices.
* Publish your [data retention policies](/platform/marketplace/cloud-security-program/).
* Develop your app to be resilient when cloud instances are slow or unavailable.
* Always [check the license parameter](/platform/marketplace/cloud-app-licensing/) on each request and observe the appropriate restrictions.

See the following pages for more details:

* [Supported platforms for Jira](https://confluence.atlassian.com/x/qgReD)
* [Supported platforms for Confluence](https://confluence.atlassian.com/x/xgReD)  
* [Supported languages for cloud applications](https://confluence.atlassian.com/x/fTIvEw)
* [Cloud application FAQ for users](https://confluence.atlassian.com/x/9QEYDw)
