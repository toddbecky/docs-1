# 1.0.1

Release date: 27th March, 2014

### Issues resolved

<table>
    <thead>
        <tr>
            <th></th>
            <th class='key'>Key</th>
            <th>Summary</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td></td>
            <td><a href="https://ecosystem.atlassian.net/browse/AC-1015">AC-1015</a></td>
            <td>Allow POST method for "screens/addToDefault/{fieldId}"</td>
        </tr>
        <tr>
            <td><span>Fixed</span></td>
            <td><a href="https://ecosystem.atlassian.net/browse/AC-1028">AC-1028</a></td>
            <td>UPM auto-update fails to upgrade app</td>
        </tr>
    </tbody>
</table>

### Testing

You can start Jira or Confluence with Atlassian Connect as follows:

#### Jira

``` bash
atlas-run-standalone --product jira --version 6.3-OD-01-067 --bundled-plugins com.atlassian.plugins:atlassian-connect-plugin:1.0.1,com.atlassian.jwt:jwt-plugin:1.0.0,com.atlassian.bundles:json-schema-validator-atlassian-bundle:1.0-m0 --jvmargs -Datlassian.upm.on.demand=true
```

#### Confluence

``` bash
atlas-run-standalone --product confluence --version 5.4-OD-20-007 --bundled-plugins com.atlassian.plugins:atlassian-connect-plugin:1.0.1,com.atlassian.jwt:jwt-plugin:1.0.0,com.atlassian.bundles:json-schema-validator-atlassian-bundle:1.0-m0 --jvmargs -Datlassian.upm.on.demand=true
```
