---
title: "Basic auth for REST APIs"
platform: cloud
product: confcloud
category: devguide
subcategory: security
date: "2017-04-14"
---
# Basic auth for REST APIs

This page shows you how to allow REST clients to authenticate themselves using 
[basic authentication](http://en.wikipedia.org/wiki/Basic_access_authentication)
(user name and password or API token).

## Overview

Confluence's REST API is protected by the same restrictions which are provided via Confluence's standard web interface. 
This means that if you do not log in, you are accessing Confluence anonymously. Furthermore, if you log in and do not 
have permission to view something in Confluence, you will not be able to view it using the Confluence REST API either.

In most cases, the first step in using the Confluence REST API is to authenticate a user account with your Confluence 
site. Any authentication that works against Confluence will work against the REST API. On this page we will show you 
a simple example of basic authentication.

## Simple example

Most client software provides a simple mechanism for supplying a user name and password (or API token) and will build the required 
authentication headers automatically. For example, you can specify the `-u` argument with cURL as follows:

``` bash
curl -D- \
   -u user@example.com:user_api_token \
   -X GET \
   -H "Content-Type: application/json" \
   https://your-domain.atlassian.net/rest/api/2/issue/createmeta
```

### Using Postman

You can use Postman to make calls to the Confluence Cloud REST APIs. Check it out: 
[Confluence Cloud REST API](/cloud/confluence/rest/).

## Supplying basic auth headers

If you need to you may construct and send basic auth headers yourself. To do this you need to perform the following steps:

 1. Generate an API Token for your Atlassian Account: https://id.atlassian.com/manage/api-tokens
 1. Build a string of the form `user@example.com:api_token`.
 1. Encode the string using base64.
 1. Supply an `Authorization` header with content `Basic ` followed by the encoded string. For example, the string `user:user` encodes to `dXNlcjp1c2Vy` in base64, so you would make the request as follows:

``` bash
curl -D- \
   -X GET \
   -H "Authorization: Basic dXNlcjp1c2Vy" \
   -H "Content-Type: application/json" \
   "https://your-domain.atlassian.net/rest/api/2/issue/QA-31"
```

## Advanced topics

#### Authentication challenges

Because Confluence permits a default level of access to anonymous users, it does not supply a typical authentication 
challenge. Some HTTP client software expect to receive an authentication challenge before they will send an authorization 
header. This means that Confluence may not behave as your HTTP client software expects. In this case, you may need to 
configure it to supply the authorization header, as described above, rather than relying on its default mechanism.